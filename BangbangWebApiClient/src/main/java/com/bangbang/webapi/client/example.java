package com.bangbang.webapi.client;

import com.bangbang.webapi.client.model.User;

import javax.ws.rs.Consumes;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by wisp on 3/23/14.
 */
public class example {
    private static final URI BASE_URI = URI.create("http://localhost:8080/bangbang/");
    private static final String TEST_AUTH = "Wa6SeqOuU8Oz1q1KKHbqUj4lIrwMhiNrD1Z6vTKB8YZbibNTFztDjG8s8RqLg+beiWyxPQvtmUcNd1CDu0EheTpaMz8r8EL9Ly17hN8nGYWgpXw0s/lRPqDJFNgx6NxB7ee9HeIVKFmAitivaosARLruDU5nQ/uASH4q0j9Ak3Q=";

    @Consumes(MediaType.APPLICATION_JSON)
    public static void main(String[] args) {
        System.out.println("Bangbang client start");

        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(BASE_URI);

        int userId = 1;
        WebTarget userWebTarget = webTarget.path("user/" + userId);


        Invocation.Builder invocationBuilder = userWebTarget.request(MediaType.APPLICATION_JSON);
        invocationBuilder.header("X-bangbang-Auth", TEST_AUTH);
        Response response = invocationBuilder.get();
        
        System.out.println(response.getStatus());
        System.out.println(response.readEntity(User.class));

    }
}
