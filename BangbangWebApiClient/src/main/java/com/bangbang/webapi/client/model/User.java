package com.bangbang.webapi.client.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.String;

/**
 * Created by wisp on 3/23/14.
 */

public class User {
    public int id;
    public String nickName;
    public String description;
    public boolean male;
    public int age;
    public String create_at;
    public String modify_at;
    public String emailAddress;
    public String city;
    public String image;
}
